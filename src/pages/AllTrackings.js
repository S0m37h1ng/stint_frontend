import TrackingService from "../components/trackings/service/TrackerService"
import TrackingItemList from "../components/trackings/TrackingItemList";
import { useState,useEffect } from 'react';

function AllTrackingsPage() {
    const [trackings,setTrackings] = useState([]);
    const [isLoading,setIsLoading] = useState(true);

    useEffect(() => {
        setIsLoading(true);
        new TrackingService().fetchTrackings()
        .then(result => {setTrackings(result)})
        .then(() => {setIsLoading(false)})
    },[])
    
    if (isLoading){
        return (
            <section>
                <h1>loading ...</h1>
            </section>
        )
    }

    else{
        return (
            <section>
                <TrackingItemList trackings={trackings} />
            </section>
        )
    }
}

export default AllTrackingsPage;