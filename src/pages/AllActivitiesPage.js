import ActivityItemList from "../components/activities/ActivityItemList";
import ActivityService from "../components/activities/services/ActivityService";
import { useState, useEffect } from 'react';
import { getCurrentDate } from "../components/utilities/UtilityService";


function AllActivitiesPage(props){
    const [activities,setActivities] = useState([]);
    const [isLoading,setIsLoading] = useState(true);
    const activityService = new ActivityService();
    const toDay = getCurrentDate();
    useEffect(() => {
        setIsLoading(true)
        activityService.getByDate(toDay)
        .then((result) => setActivities(result))
        .then(() => setIsLoading(false))
    },[])
    
    if (isLoading){
        return (
            <section>
                <h1>Loading ...!</h1>
            </section>
        )
    }
    return (  
        
        <section>
            <h1>All Activities</h1>
            <ActivityItemList activities={activities} />
        </section>
    )
}

export default AllActivitiesPage;