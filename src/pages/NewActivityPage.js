import NewActivityForm from "../components/activities/NewActivityForm";
import ActivityService from "../components/activities/services/ActivityService";

function NewActivityPage(){
    const activityService = new ActivityService();
    function saveActivity(activity){
        activityService.saveActivity(activity);
    }
    return (
        <section>
            <h1>add new activity</h1>
            <NewActivityForm onSave={saveActivity}></NewActivityForm>
        </section> 
    )
}

export default NewActivityPage;