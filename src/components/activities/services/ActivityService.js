import { urls } from '../../../config/env-config';
import { getTimeFormatFromSeconds } from '../../utilities/UtilityService';

class ActivityService {
    
    getAllActivities(){
        return fetch(urls.local_host)
        .then(result => result.json())
    }

    getByDate(date){
        return fetch(
            `${urls.local_host}/trackers/${date}`
        )
        .then(result => result.json())
        .then(result => {
            const activities = [];
            for (const key in result){
                result[key]["time"] = getTimeFormatFromSeconds(result[key]["time"])
                activities.push(result[key])
            }
            return activities;
        })
    }

    saveActivity(activity){
        fetch(
            urls.local_host + '/add',
            {
                method: "POST",
                body: JSON.stringify(activity),
                headers:{
                    'Content-Type':'application/json'
                }
            }
        )
    }

}

export default ActivityService;