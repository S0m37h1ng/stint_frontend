import classes from "./ActivityItem.module.css"
import React , {useState} from 'react'
import TrackingService from "../trackings/service/TrackerService";
import { getCurrentDate, getCurrentTime } from "../utilities/UtilityService";

function ActivityItem (props) {
    const [isActive , setActive] = useState(false);
    const [tracking,setTracking] = useState({});

    function onClickHandler() {
        if (isActive === true) {
            tracking["endTime"] = getCurrentTime();
            const trackingService = new TrackingService();
            trackingService.saveTracking(tracking);
            setActive(false);
        }
        else {
            tracking["activityId"] = props.activity.id;
            tracking["date"] = getCurrentDate();
            tracking["startTime"] = getCurrentTime();
            setActive(true);
        }
    }
    return (
            <div className={classes.item}>
                <div className={classes.content}>
                    <h1>{props.activity.name}</h1>
                    <h3>{props.activity.time}</h3>
                </div>
                
                <div className={classes.actions}>
                    <button onClick={onClickHandler}> { isActive === true  ? "Stop" : "Start" } </button>
                </div>
            </div>
    )
}

export default ActivityItem;