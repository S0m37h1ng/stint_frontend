import classes from "./NewActivityForm.module.css"
import { useRef } from 'react';

function NewActivityForm(props) {
    const nameRef = useRef();
    const imageRef = useRef();

    function onSubmitHandler(event){
        event.preventDefault();
        const activity = {
            name: nameRef.current.value,
            image: imageRef.current.value
        }
        props.onSave(activity);
    }
    return(
        <form className={classes.form} onSubmit={onSubmitHandler}>
            <div className={classes.control}>
                <label htmlFor='activityName'>Activity Name</label>
                <input type='text' required id='activityName' ref={nameRef} />
            </div>
            <div className={classes.control}>
                <label htmlFor="image">Image</label>
                <input type='text' id='image' ref={imageRef}/>
            </div>
            <div className={classes.actions}>
                <button>Add Activity</button>
            </div>
        </form>
    )
}

export default NewActivityForm;