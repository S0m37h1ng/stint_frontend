import ActivityItem from "./ActivityItem"
import classes from "./ActivityItemList.module.css"
import React from 'react'
function ActivityItemList(props) {
    return (
        <ul className={classes.list}>
            {
                props.activities.map( (activity) => {
                    return ( 
                        <li key={activity.name}> 
                            <ActivityItem activity={activity}></ActivityItem> 
                        </li>
                    )
                })
            }
        </ul>
    )
}

export default ActivityItemList;