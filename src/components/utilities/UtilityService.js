export function getCurrentDate(){
    const time = new Date();
    return (`${time.getFullYear()}-${time.getMonth() + 1}-${time.getDate()}`)
}

export function getCurrentTime(){
    const time = new Date();
    return (`${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`)
}

export function getTimeFormatFromSeconds(seconds){
    const hours = Math.floor(seconds / (3600));
    const minutes = Math.floor((seconds % 3600) / (60));
    seconds = seconds % 60;
    return `${hours}:${minutes}:${seconds}`
}