import classes from "../activities/ActivityItem.module.css"

function TrackingItem(props){
    return (
        <tr>
            <td>{props.tracking.activity.name}</td>
            <td>{props.tracking.date}</td>
            <td>{props.tracking.startTime}</td>
            <td>{props.tracking.endTime}</td>
        </tr>
    )
}

export default TrackingItem;