import { urls } from "../../../config/env-config";

class TrackingService{
    fetchTrackings(){
        return fetch(urls.local_host + "/trackers")
        .then(result =>  result.json())
    }
    
    saveTracking(traking){
        console.log(traking);
        fetch(urls.local_host + "/trackers/add",{
            method: "POST",
            body: JSON.stringify(traking),
            headers: {
                'Content-Type':'application/json'
            }
        })
    }
}

export default  TrackingService;