import classes from "./TrackingItemList.module.css"
import TrackingItem from "./TrackingItem"

function TrackingItemList(props) {
    return (
        <table className={classes.trackings} >
            <caption>All Trackings</caption>
            <thead>
                <tr>
                    <th>Activity Name</th>
                    <th>Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                </tr> 
            </thead>
            <tbody>
                {
                    props.trackings.map((tracking) => {
                        return (
                                <TrackingItem tracking={tracking} key={tracking.id}/>
                        )
                    })
                }
            </tbody>
        </table>
    )
}

export default TrackingItemList;