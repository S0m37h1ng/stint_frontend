import { Route } from 'react-router-dom';
import './App.css';
import AllActivitiesPage from './pages/AllActivitiesPage';
import React from 'react'
import { Switch } from 'react-router-dom';
import NewActivityPage from './pages/NewActivityPage';
import AllTrackingsPage from './pages/AllTrackings';

function App() {
  return (
    <Switch>
      <Route path="/" exact > <AllActivitiesPage /> </Route>
      <Route path='/activity/add'> <NewActivityPage/> </Route>
      <Route path="/trackers"><AllTrackingsPage /> </Route>
    </Switch>
  );
}

export default App;
